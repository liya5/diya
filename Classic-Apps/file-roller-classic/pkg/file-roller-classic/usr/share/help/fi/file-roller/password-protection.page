<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="password-protection" xml:lang="fi">

  <info>
    <title type="sort" its:translate="no">2</title>
    <link type="guide" xref="index#advanced-options"/>
    <link type="seealso" xref="archive-create"/>
    <link type="seealso" xref="archive-open"/>
    <revision pkgversion="3.4" date="2012-01-10" status="review"/>
    <revision pkgversion="3.6" date="2012-08-05" status="review"/>
    <revision pkgversion="3.8" date="2013-03-25" status="candidate"/>
    <revision pkgversion="3.34" date="2020-02-09" status="candidate"/>

    <credit type="author">
      <name>Marta Bogdanowicz</name>
      <email>majus85@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>
    <desc>Suojaa arkisto salasanalla.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sami Jaktholm</mal:name>
      <mal:email>sjakthol@outlook.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>Salasanasuojaus</title>

  <p>Arkiston voi salata salasanalla, jonka ansiosta vain henkilöt, ketkä tietävät salasanan, voivat avata arkiston. Tietoturvan kannalta on tärkeä käyttää <link href="help:gnome-help/user-goodpassword">hyvää salasanaa</link>, sillä arkiston salasanaa voidaan yrittää arvata.</p>

  <note style="warning">
    <p><app>Pakettienkäsittelijä</app> sallii arkiston salauksen vain tietyissä tapauksissa.</p>
  </note>

  <p>Uuden arkiston sisällön voi salata seuraavasti:</p>

  <steps>
    <item>
      <p>Aloita <link xref="archive-create">uuden arkiston luominen</link>.</p>
    </item>
    <item>
      <p>At the bottom of the file chooser dialog, expand <gui>Other Options</gui>.</p>
    </item>
    <item>
      <p>Enter a password into the <gui style="input">Password</gui> field.</p>
      <note>
        <p>Jos haluat salata tiedostoluettelon, valitse <gui style="checkbox">Salaa myös tiedostolista</gui>.</p>
      </note>
      <note style="warning">
	<p>Salasanan kirjoittaminen ei välttämättä onnistu, sillä kaikki arkistomuodot eivät tue salausta. Valitse siis salausta tukeva arkistomuoto ennen salasanan asettamista.</p>
      </note>
    </item>
    <item>
      <p>Jatka <link xref="archive-create">uuden arkiston luomista</link>.</p>
    </item>
  </steps>

  <p>Jo olemassa olevan arkiston suojaaminen salasanalla:</p>
  <steps>
    <item>
      <p><link xref="archive-open">Avaa arkisto</link>.</p>
    </item>
    <item>
      <p>Press the menu button in the top-right corner of the window and select
      <gui style="menuitem">Password…</gui>.</p>
    </item>
    <item>
      <p>Enter a password.</p>
      <note>
        <p>If you want to encrypt the list of files tick
        <gui style="checkbox">Encrypt the file list</gui>.</p>
      </note>
    </item>
    <item>
      <p>Click <gui style="button">Save</gui> to continue.</p>
      <note style="warning">
        <p><app>Pakettienkäsittelijä</app> salaa vain arkistoon lisättävät tiedostot!</p>
      </note>
    </item>
  </steps>

</page>

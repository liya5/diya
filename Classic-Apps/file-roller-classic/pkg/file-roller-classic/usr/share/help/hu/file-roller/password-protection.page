<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="password-protection" xml:lang="hu">

  <info>
    <title type="sort" its:translate="no">2</title>
    <link type="guide" xref="index#advanced-options"/>
    <link type="seealso" xref="archive-create"/>
    <link type="seealso" xref="archive-open"/>
    <revision pkgversion="3.4" date="2012-01-10" status="review"/>
    <revision pkgversion="3.6" date="2012-08-05" status="review"/>
    <revision pkgversion="3.8" date="2013-03-25" status="candidate"/>
    <revision pkgversion="3.34" date="2020-02-09" status="candidate"/>

    <credit type="author">
      <name>Marta Bogdanowicz</name>
      <email>majus85@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>
    <desc>Az archívum jelszavának beállítása.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2012, 2013, 2014, 2016.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Úr Balázs</mal:name>
      <mal:email>ur.balazs at fsf dot hu</mal:email>
      <mal:years>2014, 2018, 2021.</mal:years>
    </mal:credit>
  </info>

  <title>Jelszavas védelem</title>

  <p>Az archívumokat titkosíthatja, így csak Ön és azok érhetik el, akikkel megosztja a jelszót. Ne feledje, hogy a jelszót akár ki is lehet találni, így a legnagyobb biztonság érdekében használjon <link href="help:gnome-help/user-goodpassword">erős jelszót</link>.</p>

  <note style="warning">
    <p>Az <app>Archívumkezelő</app> csak speciális körülmények között teszi lehetővé az archívumok titkosítását.</p>
  </note>

  <p>Egy új archívum adatainak titkosítására használandó jelszó beállítása:</p>

  <steps>
    <item>
      <p>Kezdjen el <link xref="archive-create">létrehozni egy új archívumot</link>.</p>
    </item>
    <item>
      <p>A fájlválasztó párbeszédablak alján nyissa ki az <gui>Egyéb beállításokat</gui>.</p>
    </item>
    <item>
      <p>Adja meg a jelszót a <gui style="input">Jelszó</gui> mezőben.</p>
      <note>
        <p>Ha a fájlok listáját is titkosítani szeretné, akkor jelölje be a <gui style="checkbox">Fájllista titkosítása</gui> jelölőnégyzetet.</p>
      </note>
      <note style="warning">
	<p>Előfordulhat, hogy nem tud beírni jelszót, mert nem minden archívumtípus támogatja a titkosítás, ezért a jelszó megadása előtt válasszon egy jelszóval védhető archívumformátumot.</p>
      </note>
    </item>
    <item>
      <p>Folytassa az <link xref="archive-create">új archívum létrehozását</link>.</p>
    </item>
  </steps>

  <p>Meglévő archívum jelszavas védelmének beállítása:</p>
  <steps>
    <item>
      <p><link xref="archive-open">Archívum megnyitása</link>.</p>
    </item>
    <item>
      <p>Nyomja meg az ablak jobb felső sarkában lévő menügombot, és válassza a <gui style="menuitem">Jelszó…</gui> menüpontot.</p>
    </item>
    <item>
      <p>Adjon meg egy jelszót.</p>
      <note>
        <p>Ha titkosítani szeretné a fájlok listáját, akkor jelölje be a <gui style="checkbox">Fájllista titkosítása</gui> jelölőnégyzetet.</p>
      </note>
    </item>
    <item>
      <p>Nyomja meg a <gui style="button">Mentés</gui> gombot a folytatáshoz.</p>
      <note style="warning">
        <p>Az <app>Archívumkezelő</app> csak az újonnan az archívumhoz adott fájlokat fogja titkosítani!</p>
      </note>
    </item>
  </steps>

</page>

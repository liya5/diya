<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="archive-extract-advanced-options" xml:lang="pt_BR">

  <info>
    <title type="sort" its:translate="no">1</title>
    <link type="guide" xref="index#advanced-options"/>
    <revision pkgversion="3.4" date="2012-03-06" status="review"/>
    <revision pkgversion="3.6" date="2012-08-05" status="review"/>
    <revision pkgversion="3.8" date="2013-03-25" status="candidate"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.34" date="2020-02-09" status="candidate"/>

    <credit type="author">
      <name>Marta Bogdanowicz</name>
      <email>majus85@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Especifique suas preferências para extração de pacotes.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fábio Nogueira</mal:name>
      <mal:email>fnogueira@gnome.org</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Opções avançadas para extração de pacotes</title>

  <p>O <app>Gerenciador de compactação</app> oferece diferentes opções para a <link xref="archive-extract">extração de um pacote</link>. Você pode ver essas opções no diálogo de <gui>Extrair</gui>. No canto inferior deste diálogo, você pode decidir se deseja extrair:</p>

  <terms>
    <item>
      <title><gui>Todos arquivos</gui></title>
      <p>Todos os arquivos e pastas no pacote serão extraídas.</p>
    </item>
    <item>
      <title><gui>Arquivos selecionados</gui></title>
      <p>O <app>Gerenciador de compactação</app> extrai somente os arquivos selecionados.</p>
      <note>
        <p>Você precisa selecionar os arquivos que você deseja extrair antes de clicar em <gui style="button">Extrair</gui>. Faça isso clicando no nome do arquivo. Use as teclas <key>Ctrl</key> e <key>Shift</key> para selecionar mais de um arquivo.</p>
      </note>
    </item>
    <item>
      <title><gui>Arquivos</gui></title>
      <p>Você pode digitar os nomes dos arquivos que você deseja extrair. Separe arquivos individuais usando ponto-e-vírgula (<key>;</key>).</p>
      <note>
        <p>O nome do arquivo deve ser seguido por uma extensão de arquivo. Você pode usar o asterisco (<key>*</key>) como uma carta coringa. Por exemplo, para selecionar todos arquivos <file>.txt</file>, digite <file>*.txt</file>.</p>
      </note>
    </item>
  </terms>

  <p>Você pode especificar no diálogo do seletor de arquivos se você deseja:</p>
  <terms>
    <item>
      <title><gui>Manter a estrutura do diretório</gui></title>
      <p>Marque esta opção se você desejar manter a estrutura do diretório como está no seu pacote.</p>
    </item>
    <item>
      <title><gui>Não sobrescrever arquivos mais novos</gui></title>
      <p>Esta opção não irá sobrescrever arquivos existentes com o mesmo nome, que tenham uma data de modificação mais recente do que aqueles do pacote.</p>
    </item>
  </terms>

  <p>Essas <gui>Ações</gui> estão listadas no canto inferior do diálogo.</p>

</page>

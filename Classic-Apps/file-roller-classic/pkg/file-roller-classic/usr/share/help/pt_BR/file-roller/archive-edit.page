<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="archive-edit" xml:lang="pt_BR">

  <info>
    <title type="sort" its:translate="no">3</title>
    <link type="guide" xref="index#managing-archives"/>
    <link type="seealso" xref="archive-create"/>
    <revision pkgversion="3.4" date="2011-12-08" status="review"/>
    <revision pkgversion="3.6" date="2012-08-05" status="review"/>
    <revision pkgversion="3.8" date="2013-03-25" status="candidate"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.34" date="2020-02-09" status="candidate"/>

    <credit type="author">
      <name>Marta Bogdanowicz</name>
      <email>majus85@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Altere o conteúdo do seu pacote.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fábio Nogueira</mal:name>
      <mal:email>fnogueira@gnome.org</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Editar um pacote</title>

  <p>Com o <app>Gerenciador de compactação</app> você pode editar pacotes existentes adicionando novos arquivos, removendo indesejados ou renomeando-os. Você pode trabalhar com pastas da mesma forma que faz com arquivos.</p>

  <terms>
    <item>
      <title>Adicionar arquivos</title>
      <p>Adicione arquivos a um pacote existente seguindo as instruções para <link xref="archive-create">criação de pacotes</link>.</p>
    </item>
    <item>
      <title>Remover arquivos</title>
      <p>Clique com botão direito no arquivo e escolha <gui style="menuitem">Excluir</gui> ou pressione <key>Delete</key>.</p>
    </item>
    <item>
    <title>Renomear arquivos</title>
      <p>Clique com botão direito no arquivo e escolha <gui style="menuitem">Renomear…</gui> ou pressione <key>F2</key>. Digite o novo nome de arquivo na janela de diálogo que estiver aberta e confirme o novo nome.</p>
    </item>
  </terms>

</page>

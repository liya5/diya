<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="archive-create" xml:lang="pt_BR">

  <info>
    <title type="sort" its:translate="no">1</title>
    <link type="guide" xref="index#managing-archives"/>
    <revision pkgversion="3.4" date="2012-01-18" status="review"/>
    <revision pkgversion="3.6" date="2012-08-05" status="review"/>
    <revision pkgversion="3.8" date="2013-03-25" status="candidate"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.34" date="2020-02-09" status="candidate"/>

    <credit type="author">
      <name>Marta Bogdanowicz</name>
      <email>majus85@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Adicione arquivos ou pastas a um novo pacote.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013, 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2012-2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fábio Nogueira</mal:name>
      <mal:email>fnogueira@gnome.org</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Criar um novo pacote</title>

  <p>Crie um novo pacote com o <app>Gerenciador de compactação</app> seguindo os passos abaixo:</p>

  <steps>
    <item>
      <p>Pressione o botão de menu no canto superior direito da janela e selecione <gui style="menuitem">Novo pacote</gui>.</p>
    </item>
    <item>
      <p>Nomeie seu novo pacote e selecione o formato do arquivo na lista suspensa.</p>
    </item>
    <item>
      <p>Escolha o local onde o arquivo do pacote será salvo.</p>
    </item>
    <item>
      <p>Clique em <gui>Criar</gui> para continuar.</p>
      <note style="tip">
	<p>Ao clicar em <gui>Outras opções</gui> você pode definir uma senha ou dividir seu novo pacote em diversos arquivos menores, selecionando a opção relevante e especificando o volume para cada parte em <gui>MB</gui>. Essas opções somente estarão disponíveis se o formato do arquivo de pacote for compatível.</p>
      </note>
    </item>
    <item>
      <p>Adicione arquivos e pastas desejados ao seu pacote pressionando <gui>+</gui> na barra de cabeçalho. Verifique a caixa próxima aos arquivos e pastas que você deseja adicionar.</p>
      <note style="tip">
	<p>Nem todos formatos de arquivos de pacotes têm suporte a pastas — se o formato de arquivo que você estiver usando não tiver suporte, você não será avisado. Se o formato de arquivo que você estiver usando não tiver suporte a pastas, os arquivos dos diretórios serão adicionados, mas não a pasta em si.</p>
      </note>
    </item>
    <item>
      <p>Uma vez finalizada a adição de arquivos, o arquivo está pronto — você não precisa salvá-lo.</p>
    </item>
  </steps>

</page>

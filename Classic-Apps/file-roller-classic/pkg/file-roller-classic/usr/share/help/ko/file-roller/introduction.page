<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="introduction" xml:lang="ko">

  <info>
    <title type="sort" its:translate="no">1</title>
    <link type="guide" xref="index"/>
    <revision pkgversion="3.4" date="2012-01-18" status="review"/>
    <revision pkgversion="3.6" date="2012-08-05" status="review"/>
    <revision pkgversion="3.8" date="2013-03-25" status="candidate"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.34" date="2020-02-09" status="candidate"/>

    <credit type="author">
      <name>Marta Bogdanowicz</name>
      <email>majus85@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>그놈 <app>압축 관리자</app>를 소개합니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2016, 2021.</mal:years>
    </mal:credit>
  </info>

  <title>소개</title>

  <p><app>압축 관리자</app>는 <file>.zip</file> 또는 <file>.tar</file> 파일과 같은 압축 파일을 관리하는 프로그램이며, 사용하기 쉽게 설계했습니다. <app>압축 관리자</app>는 압축 파일을 만들고, 수정하고, 풀어내는 모든 도구를 갖추었습니다.</p>

  <note style="advanced">
    <p>압축 파일은 하나 이상의 파일, 폴더로 구성하며, 메타데이터도 따라옵니다. 일부 또는 전체를 암호화합니다. 압축 파일은 여러 파일을 하나로 모을 수 있어 컴퓨터간 저장 데이터를 전송하는데 요긴하게 쓸 수 있습니다.</p>
  </note>

  <p><app>압축 관리자</app> 에서 다음 기능을 활용할 수 있습니다:</p>
  <list>
    <item><p>새 압축 파일 만들기</p></item>
    <item><p>기존 압축 파일 내용 보기</p></item>
    <item><p>압축 파일에 들어있는 파일 보기</p></item>
    <item><p>기존 압축 파일 수정</p></item>
    <item><p>압축 파일에서 파일 풀어내기</p></item>
  </list>

</page>

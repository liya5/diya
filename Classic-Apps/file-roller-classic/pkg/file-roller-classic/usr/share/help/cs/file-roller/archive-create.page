<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="archive-create" xml:lang="cs">

  <info>
    <title type="sort" its:translate="no">1</title>
    <link type="guide" xref="index#managing-archives"/>
    <revision pkgversion="3.4" date="2012-01-18" status="review"/>
    <revision pkgversion="3.6" date="2012-08-05" status="review"/>
    <revision pkgversion="3.8" date="2013-03-25" status="candidate"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.34" date="2020-02-09" status="candidate"/>

    <credit type="author">
      <name>Marta Bogdanowicz</name>
      <email>majus85@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Jak přidat soubory a složky do nového archivu.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2009, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jakub Mach</mal:name>
      <mal:email>jakub.m2011@ch.gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Vytvoření nového archivu</title>

  <p>Nový archiv vytvoříte pomocí aplikace <app>Správa archivů</app> v následujících krocích:</p>

  <steps>
    <item>
      <p>Zmáčkněte tlačítko nabídky v pravém horním rohu okna a vyberte <gui style="menuitem">Nový archiv</gui>.</p>
    </item>
    <item>
      <p>Nazvěte soubor se svým novým archivem a v rozbalovacím seznamu vyberte formát.</p>
    </item>
    <item>
      <p>Vyberte umístění, kam se má soubor s archivem uložit.</p>
    </item>
    <item>
      <p>Pokračujte zmáčknutím <gui>Vytvořit</gui>.</p>
      <note style="tip">
	<p>Po kliknutí na <gui>Další volby</gui> můžete nastavit heslo nebo rozdělit nový archiv na více malých jednotlivých souborů tak, že vyberete odpovídající volbu a určíte velikost každé části v <gui>MB</gui>. Tato volby je ale dostupná jen v případě, že ji vybraný formát archivu podporuje.</p>
      </note>
    </item>
    <item>
      <p>Zmáčknutím <gui>+</gui> na hlavičkové liště přidejte požadované soubory a složky. Stačí zaškrtnout políčka vedle těch, které chcete přidat.</p>
      <note style="tip">
	<p>Ne všechny formáty souborů s archivy podporují složky. Pokud je formát, který jste použili, nepodporuje, nebudete nijak varování. V takovém případě budou soubory ze složky přidány, ale složka samotná ne.</p>
      </note>
    </item>
    <item>
      <p>Až dokončíte přidávání souborů, je archiv připraven. Není potřeba jej ukládat.</p>
    </item>
  </steps>

</page>

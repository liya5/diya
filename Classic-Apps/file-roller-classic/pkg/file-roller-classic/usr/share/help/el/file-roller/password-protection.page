<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="password-protection" xml:lang="el">

  <info>
    <title type="sort" its:translate="no">2</title>
    <link type="guide" xref="index#advanced-options"/>
    <link type="seealso" xref="archive-create"/>
    <link type="seealso" xref="archive-open"/>
    <revision pkgversion="3.4" date="2012-01-10" status="review"/>
    <revision pkgversion="3.6" date="2012-08-05" status="review"/>
    <revision pkgversion="3.8" date="2013-03-25" status="candidate"/>
    <revision pkgversion="3.34" date="2020-02-09" status="candidate"/>

    <credit type="author">
      <name>Marta Bogdanowicz</name>
      <email>majus85@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>
    <desc>Ορίστε κωδικό πρόσβασης για το συμπιεσμένο αρχείο σας.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gmail.com</mal:email>
      <mal:years>2012, 2014</mal:years>
    </mal:credit>
  </info>

  <title>Προστασία κωδικού πρόσβασης</title>

  <p>Μπορείτε να κρυπτογραφήσετε ένα συμπιεσμένο αρχείο με κωδικό,έτσι ώστε μόνο εσείς και όποιοι άλλοι επιλέξετε να μοιραστείτε τον κωδικό να μπορούν να έχουν πρόσβαση σε αυτό. Σημειώστε ότι ο κωδικός μπορεί να μαθευτεί, έτσι για μεγαλύτερη ασφάλεια, χρησιμοποιήστε έναν <link href="help:gnome-help/user-goodpassword">καλό κωδικό</link>.</p>

  <note style="warning">
    <p>Ο <app>Διαχειριστής συμπιεσμένων αρχείων</app> σας επιτρέπει να κρυπτογραφήσετε ένα συμπιεσμένο αρχείο με κωδικό μόνο σε συγκεκριμένες περιστάσεις.</p>
  </note>

  <p>Ορίστε έναν κωδικό πρόσβασης για να κρυπτογραφήσετε τα δεδομένα του συμπιεσμένου αρχείου ακολουθώντας αυτά τα βήματα:</p>

  <steps>
    <item>
      <p>Ξεκινήστε με τη <link xref="archive-create">δημιουργία ενός συμπιεσμένου αρχείου</link>.</p>
    </item>
    <item>
      <p>At the bottom of the file chooser dialog, expand <gui>Other Options</gui>.</p>
    </item>
    <item>
      <p>Enter a password into the <gui style="input">Password</gui> field.</p>
      <note>
        <p>Αν θέλετε να κρυπτογραφήσετε τη λίστα των αρχείων επιλέξτε <gui style="checkbox">Κρυπτογράφηση και της λίστας αρχείων</gui>.</p>
      </note>
      <note style="warning">
	<p>Μπορεί να μη μπορέσετε να πληκτρολογήσετε ένα κωδικό, διότι δεν υποστηρίζουν όλοι οι τύποι αρχείου κρυπτογράφηση, γι' αυτό είναι καλύτερα πριν τον ορισμό του κωδικού να επιλέξετε έναν τύπο αρχειοθήκης που μπορεί να προστατεύεται με κωδικό.</p>
      </note>
    </item>
    <item>
      <p>Συνεχίστε με τη <link xref="archive-create">δημιουργία ενός νέου συμπιεσμένου αρχείου</link>.</p>
    </item>
  </steps>

  <p>Προστατέψτε ένα υπάρχον συμπιεσμένο αρχείο ορίζοντας έναν κωδικό:</p>
  <steps>
    <item>
      <p><link xref="archive-open">Ανοίξτε ένα συμπιεσμένο αρχείο</link>.</p>
    </item>
    <item>
      <p>Press the menu button in the top-right corner of the window and select
      <gui style="menuitem">Password…</gui>.</p>
    </item>
    <item>
      <p>Enter a password.</p>
      <note>
        <p>If you want to encrypt the list of files tick
        <gui style="checkbox">Encrypt the file list</gui>.</p>
      </note>
    </item>
    <item>
      <p>Click <gui style="button">Save</gui> to continue.</p>
      <note style="warning">
        <p>Ο <app>Διαχειριστής συμπιεσμένων αρχείων</app> θα κρυπτογραφήσει μόνο τα νέα αρχεία που θα προστεθούν στο συμπιεσμένο αρχείο!</p>
      </note>
    </item>
  </steps>

</page>

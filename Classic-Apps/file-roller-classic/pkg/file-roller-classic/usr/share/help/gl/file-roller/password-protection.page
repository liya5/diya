<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="password-protection" xml:lang="gl">

  <info>
    <title type="sort" its:translate="no">2</title>
    <link type="guide" xref="index#advanced-options"/>
    <link type="seealso" xref="archive-create"/>
    <link type="seealso" xref="archive-open"/>
    <revision pkgversion="3.4" date="2012-01-10" status="review"/>
    <revision pkgversion="3.6" date="2012-08-05" status="review"/>
    <revision pkgversion="3.8" date="2013-03-25" status="candidate"/>
    <revision pkgversion="3.34" date="2020-02-09" status="candidate"/>

    <credit type="author">
      <name>Marta Bogdanowicz</name>
      <email>majus85@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <license>
      <p>Creative Commons Compartir Igual 3.0</p>
    </license>
    <desc>Estabelecer o contrasinal dun arquivo.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Diéguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2012-2021</mal:years>
    </mal:credit>
  </info>

  <title>Protección por contrasinal</title>

  <p>Pode cifrar un arquivo cun contrasinal para que só vostede e aqueles que desexe poidan acceder a el. Teña en conta que o contrasinal pode ser adiviñado polo que para unha maior seguranza, use un <link href="help:gnome-help/user-goodpassword">bo contrasinal</link>.</p>

  <note style="warning">
    <p>O <app>Xestor de arquivos</app> permítelle cifrar un arquivo cun contrasinal só en determinadas circunstancias.</p>
  </note>

  <p>Estabeleza un contrasinal para cifrar os datos nun novo arquivo seguinte os seguintes pasos:</p>

  <steps>
    <item>
      <p>Comece <link xref="archive-create">creando un arquivo novo</link>.</p>
    </item>
    <item>
      <p>Na parte inferior do diálogo selector de ficheiros expanda <gui>Outras opcións</gui>.</p>
    </item>
    <item>
      <p>Escriba un contrasinal no campo <gui style="input">Contrasinal</gui>.</p>
      <note>
        <p>Se quere cifrar tamén a lista de ficheiros, marque <gui style="checkbox">Cifrar tamén a lista de ficheiros</gui>.</p>
      </note>
      <note style="warning">
	<p>É posíbel que non poida escribir un contrasinal xa que non todos os tipos de arquivos admiten cifrado polo que é mellor que seleccione un formato de ficheiro que poida protexerse con contrasinal antes de estabelecer un.</p>
      </note>
    </item>
    <item>
      <p>Continúe <link xref="archive-create">creando un arquivo novo</link>.</p>
    </item>
  </steps>

  <p>Protexer un arquivo existente usando un contrasinal:</p>
  <steps>
    <item>
      <p><link xref="archive-open">Abrir un arquivo</link>.</p>
    </item>
    <item>
      <p>Prema o botón de menú na esquina superior dereita da xanela e seleccione <gui style="menuitem">Contrasinal…</gui>.</p>
    </item>
    <item>
      <p>Escriba un contrasinal.</p>
      <note>
        <p>Se quere cifrar a lista de ficheiros, marque <gui style="checkbox">Cifrar a lista de ficheiros</gui>.</p>
      </note>
    </item>
    <item>
      <p>Prema <gui style="button">Gardarz</gui> para continuar.</p>
      <note style="warning">
        <p>O <app>Xestor de arquivos</app> só cifrará os ficheiros novos que se engadan ao arquivo!</p>
      </note>
    </item>
  </steps>

</page>

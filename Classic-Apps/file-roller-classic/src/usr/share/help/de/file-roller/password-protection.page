<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="password-protection" xml:lang="de">

  <info>
    <title type="sort" its:translate="no">2</title>
    <link type="guide" xref="index#advanced-options"/>
    <link type="seealso" xref="archive-create"/>
    <link type="seealso" xref="archive-open"/>
    <revision pkgversion="3.4" date="2012-01-10" status="review"/>
    <revision pkgversion="3.6" date="2012-08-05" status="review"/>
    <revision pkgversion="3.8" date="2013-03-25" status="candidate"/>
    <revision pkgversion="3.34" date="2020-02-09" status="candidate"/>

    <credit type="author">
      <name>Marta Bogdanowicz</name>
      <email>majus85@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>
    <desc>Ein Passwort für Ihr Archiv festlegen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jan Arne Petersen</mal:name>
      <mal:email>jpetersen@jpetersen.org</mal:email>
      <mal:years>2006</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2008, 2009, 2011-2013, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011, 2012, 2013, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Passwortschutz</title>

  <p>Sie können ein Archiv mit einem Passwort verschlüsseln, so dass nur Sie und diejenigen, an die Sie Ihr Passwort weitergeben, Zugriff darauf erhalten. Bedenken Sie, dass das Passwort immer noch erraten werden könnte, so dass Sie für höchstmögliche Sicherheit ein <link href="help:gnome-help/user-goodpassword">gutes Passwort</link> verwenden sollten.</p>

  <note style="warning">
    <p>Die <app>Archivverwaltung</app> ermöglicht Ihnen die Verschlüsselung eines Archivs mit einem Passwort nur unter bestimmten Umständen.</p>
  </note>

  <p>Gehen Sie wie folgt vor, um die Daten in einem Archiv zu verschlüsseln:</p>

  <steps>
    <item>
      <p>Erstellen Sie <link xref="archive-create">ein neues Archiv</link>.</p>
    </item>
    <item>
      <p>Klappen Sie am unteren Rand des Dateiauswahldialogs <gui>Erweiterte Einstellungen</gui> aus.</p>
    </item>
    <item>
      <p>Geben Sie Ihr Passwort in das <gui style="input">Passwort</gui>-Feld ein.</p>
      <note>
        <p>Falls Sie die Dateiliste verschlüsseln wollen, aktivieren Sie <gui style="checkbox">Dateiliste ebenfalls verschlüsseln</gui>.</p>
      </note>
      <note style="warning">
	<p>Möglicherweise können Sie kein Passwort eingeben, weil nicht alle Archivtypen dafür geeignet sind. Daher ist es besser, wenn Sie vor dem Festlegen eines Passworts zunächst ein entsprechendes Dateiformat wählen.</p>
      </note>
    </item>
    <item>
      <p>Setzen Sie die <link xref="archive-create">Erstellung eines neuen Archivs</link> fort.</p>
    </item>
  </steps>

  <p>So schützen Sie ein vorhandenes Archiv mit einem Passwort:</p>
  <steps>
    <item>
      <p><link xref="archive-open">Öffnen</link> Sie ein Archiv.</p>
    </item>
    <item>
      <p>Klicken Sie auf den Menüknopf in der oberen rechten Ecke des Fensters und wählen Sie <gui style="menuitem">Passwort …</gui>.</p>
    </item>
    <item>
      <p>Geben Sie ein Passwort ein.</p>
      <note>
        <p>Falls Sie die Dateiliste verschlüsseln wollen, aktivieren Sie <gui style="checkbox">Dateiliste ebenfalls verschlüsseln</gui>.</p>
      </note>
    </item>
    <item>
      <p>Klicken Sie auf <gui style="button">Speichern</gui>, um fortzusetzen.</p>
      <note style="warning">
        <p>Die <app>Archivverwaltung</app> verschlüsselt nur Dateien, die neu zum Archiv hinzugefügt werden!</p>
      </note>
    </item>
  </steps>

</page>

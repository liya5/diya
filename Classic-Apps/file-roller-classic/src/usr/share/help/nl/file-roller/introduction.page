<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="introduction" xml:lang="nl">

  <info>
    <title type="sort" its:translate="no">1</title>
    <link type="guide" xref="index"/>
    <revision pkgversion="3.4" date="2012-01-18" status="review"/>
    <revision pkgversion="3.6" date="2012-08-05" status="review"/>
    <revision pkgversion="3.8" date="2013-03-25" status="candidate"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.34" date="2020-02-09" status="candidate"/>

    <credit type="author">
      <name>Marta Bogdanowicz</name>
      <email>majus85@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Aan de slag met Gnome <app>Archiefbeheer</app>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nathan Follens</mal:name>
      <mal:email>nfollens@gnome.org</mal:email>
      <mal:years>2022.</mal:years>
    </mal:credit>
  </info>

  <title>Inleiding</title>

  <p><app>Archiefbeheer</app> is een toepassing om archiefbestanden te beheren, bijvoorbeeld <file>.zip</file>- of <file>.tar</file>-bestanden; ontworpen met oog op gebruiksgemak. Met <app>Archiefbeheer</app> beschikt u over alle nodige middelen voor het aanmaken, aanpassen en uitpakken van archieven.</p>

  <note style="advanced">
    <p>Een archief bestaat uit een of meerdere bestanden en mappen, samen met metadata. Het kan gedeeltelijk of geheel versleuteld zijn. Archiefbestanden zijn nuttig om gegevens op te slaan en over te dragen tussen computers, omdat ze meerdere bestanden in één kunnen verzamelen.</p>
  </note>

  <p>Met <app>Archiefbeheer</app> kunt u:</p>
  <list>
    <item><p>een nieuw archief aanmaken</p></item>
    <item><p>de inhoud van een bestand archief bekijken</p></item>
    <item><p>een bestand in een archief bekijken</p></item>
    <item><p>bestaande archieven aanpassen</p></item>
    <item><p>bestanden uit een archief uitpakken</p></item>
  </list>

</page>

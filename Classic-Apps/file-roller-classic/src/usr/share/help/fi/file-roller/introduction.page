<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="introduction" xml:lang="fi">

  <info>
    <title type="sort" its:translate="no">1</title>
    <link type="guide" xref="index"/>
    <revision pkgversion="3.4" date="2012-01-18" status="review"/>
    <revision pkgversion="3.6" date="2012-08-05" status="review"/>
    <revision pkgversion="3.8" date="2013-03-25" status="candidate"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.34" date="2020-02-09" status="candidate"/>

    <credit type="author">
      <name>Marta Bogdanowicz</name>
      <email>majus85@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Johdanto GNOME:n <app>Pakettienkäsittelijän</app> käyttöön.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sami Jaktholm</mal:name>
      <mal:email>sjakthol@outlook.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>Johdanto</title>

  <p><app>Pakettienkäsittelijä</app> on helppokäyttöinen arkistojen, kuten <file>.zip</file> ja <file>.tar</file> -tiedostojen, hallintaan tarkoitettu sovellus. <app>Pakettienkäsittelijällä</app> voi sekä luoda uusia että muokata ja purkaa jo olemassa olevia arkistoja.</p>

  <note style="advanced">
    <p>Arkisto koostuu yhdestä tai useammasta tiedostosta ja kansiosta sekä metadatasta. Ne voivat olla joko osittain tai kokonaan salattuja. Arkistot ovat käteviä tiedon säilyttämiseen ja siirtämiseen laitteiden välillä, sillä yhteen arkistoon voi kerätä useita tiedostoja.</p>
  </note>

  <p><app>Pakettienkäsittelijällä</app> voi:</p>
  <list>
    <item><p>luoda uusia arkistoja</p></item>
    <item><p>tarkastella olemassa olevien arkistojen sisältöä</p></item>
    <item><p>avata arkistojen sisältämiä tiedostoja</p></item>
    <item><p>muokata olemassa olevia arkistoja</p></item>
    <item><p>purkaa tiedostoja arkistosta</p></item>
  </list>

</page>

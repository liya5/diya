#!/bin/bash
#-->> OS Prober
if [[ $(os-prober) ]]; then
    echo "You have another operating system installed, keeping grub as it is"
else
    echo "You don't have any other OS installed, installing grub-silent ..."
    pacman -Rdd grub --noconfirm
    pacman -U /usr/share/postinstall/grub/grub-silent-2.06-5.1-x86_64.pkg.tar.zst --noconfirm
    mv /etc/default/grub_patched_after_update /etc/default/grub
    grub-mkconfig -o /boot/grub/grub.cfg
fi
